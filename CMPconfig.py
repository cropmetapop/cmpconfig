#! usr/bin/python3

"""
This program aims at creating parameter files for CropMetaPop
"""

from tkinter import *
from tkinter import filedialog, ttk
from tkinter.ttk import *

fileInit = ['', '', '', '']
fileGenMap = ['']
fileFitness = ['', '']
fileColo = ['']
fileMigr = ['']
# paramList = list()
listeParams = ["folder:", "folder_time:", "generations:", "replicates:", "seed:", "nb_pop:", "init_size:", "nb_marker:", "nb_allele:", "carr_capacity:", "init_AlleleFrequency:", "init_AlleleFrequency_equal:", "init_GenotypeFrequency:", "init_GenotypeFrequency_equal:", "geneticMap:", "fecundity:", "percentSelf:", "mut_rate:", "optimum:", "fitness:", "fitness_equal:", "ext_rate:", "col_network:", "col_rate:", "col_directed:", "col_nb_edge:", "col_nb_cluster:", "col_prob_intra:", "col_prob_inter:", "col_power:", "col_network:", "col_transfer_model:", "col_from_one:", "migr_network:", "migr_rate:", "migr_directed:", "migr_nb_edge:", "migr_nb_cluster:", "migr_prob_intra:", "migr_prob_inter:", "migr_power:", "migr_network:", "migr_transfer_model:", "migr_from_one:", "migr_carrying:", "migr_replace:", "step:", "outputs:"]

parListClean = list()
strToPrint = list()


def createString():
    """ This function creates the string to print in the parameter file, and saves the corresponding file """
    paramList = getGeneral()
    for elem in paramList:  # on transpose les sous-listes dans la liste principale
        print(elem)
        if isinstance(elem, list):
            for subelem in elem:
                print("sub")
                parListClean.append(subelem)
        else:
            parListClean.append(elem)

    for paramId, paramName in enumerate(listeParams):  # on parcours tous les paramètres
        print(paramId, paramName, str(parListClean[paramId]))
        # affichage des sections du paramFile
        if paramId == 0:
            strToPrint.append("# Simulation parameters")
        elif paramId == 5:
            strToPrint.append("\n# Meta-Population parameters")
        elif paramId == 15:
            strToPrint.append("\n# Life Cycle parameters")
        elif paramId == 22:
            strToPrint.append("\n# Colonization parameters")
        elif paramId == 33:
            strToPrint.append("\n# Migration parameters")
        elif paramId == 46:
            strToPrint.append("\n# Output parameters")

        if paramId == 1:
            strToPrint.append(paramName+ str(parListClean[paramId]))
        elif parListClean[paramId] != '' and parListClean[paramId] != "0":
            if paramId != 47:
                if paramId in [6, 8, 9, 17, 18, 21, 45]:
                    strToPrint.append(paramName+ "{" + str(parListClean[paramId]) + "}")
                else:
                    strToPrint.append(paramName+ str(parListClean[paramId]))
            else:
                listTemp = list()
                if parListClean[paramId] == 1:
                    listTemp.append("Genotype")
                if parListClean[paramId+1] == 1:
                    listTemp.append("Haplotype")
                if parListClean[paramId+2] == 1:
                    listTemp.append("Seed_transfert")
                if len(",".join(listTemp)) > 4:
                    strToPrint.append(paramName + "{" + ",".join(listTemp) + "}")
    f = filedialog.asksaveasfile(mode='w')
    f.write("\n".join(strToPrint))
    f.close()
    window.destroy()


def getGeneral():
    """ This function gets all the entered parameters """
    paramList = [name.get(), str(foldTime.get()), generation.get(), replicates.get(), seed.get(),
                 nPop.get(), initSize.get(), nMark.get(), nAlle.get(), carrCap.get(), fileInit, fileGenMap,
                 fecund.get(), percSelf.get(), mutRate.get(), optim.get(), fileFitness,
                 extRate.get(),
                 colNet.get().split(":")[0], colRate.get(), colDirect.get().split(":")[0],
                 colnEdge.get(), colnClust.get(), colProbIntra.get(),
                 colProbInter.get(), colPower.get(), fileColo, colTrans.get(), colFromOne.get(),
                 migNet.get().split(":")[0], migRate.get(), migDirect.get().split(":")[0], mignEdge.get(), mignClust.get(), migProbIntra.get(), migProbInter.get(), migPower.get(), fileMigr, migTrans.get(), migFromOne.get(), migCarr.get().split(":")[0], migRepl.get(),
                 step.get(), outGeno.get(), outHaplo.get(), outSeedTrans.get(), sepRep.get().split(":")[0]]
    return paramList


def getColFile():
    """ This function gets the path and name of the colonization file """
    fileCol = filedialog.askopenfilename()
    colNetFileBut.configure(text=fileCol.split("/")[-1])
    fileColo[0] = fileCol


def getMigFile():
    """ This function gets the path and name of the migration file """
    fileMig = filedialog.askopenfilename()
    migNetFileBut.configure(text=fileMig.split("/")[-1])
    fileMigr[0] = fileMig


def getInitAlleFreq():
    """ This function gets the init_AlleleFrequency file """
    fileInitAlleFreq = filedialog.askopenfilename()
    initAlleFreqEq.configure(state="disabled")
    initGenoFreq.configure(state="disabled")
    initGenoFreqEq.configure(state="disabled")
    initAlleFreq.configure(text=fileInitAlleFreq.split("/")[-1])
    fileInit[0] = fileInitAlleFreq


def getInitAlleFreqEq():
    """ This function gets the init_AlleleFrequency_equal file """
    fileInitAlleFreqEq = filedialog.askopenfilename()
    initAlleFreq.configure(state="disabled")
    initGenoFreq.configure(state="disabled")
    initGenoFreqEq.configure(state="disabled")
    initAlleFreqEq.configure(text=fileInitAlleFreqEq.split("/")[-1])
    fileInit[1] = fileInitAlleFreqEq


def getInitGenoFreq():
    """ This function gets the init_GenotypeFrequency file """
    fileInitGenoFreq = filedialog.askopenfilename()
    initGenoFreqEq.configure(state="disabled")
    initAlleFreqEq.configure(state="disabled")
    initAlleFreq.configure(state="disabled")
    initGenoFreq.configure(text=fileInitGenoFreq.split("/")[-1])
    fileInit[2] = fileInitGenoFreq


def getInitGenoFreqEq():
    """ This function gets the init_GenotypeFrequency_equal file """
    fileInitGenoFreqEq = filedialog.askopenfilename()
    initGenoFreq.configure(state="disabled")
    initGenoFreqEq.configure(text=fileInitGenoFreqEq.split("/")[-1])
    fileInit[3] = fileInitGenoFreqEq


def getGenetMap():
    """ This function gets the genetic map file """
    fileGenetMap = filedialog.askopenfilename()
    genetMapBut.configure(text=fileGenetMap.split("/")[-1])
    fileGenMap[0] = fileGenetMap


def getFitEq():
    """ This function gets the fitness_equal file """
    fileFitEq = filedialog.askopenfilename()
    fitBut.configure(state="disabled")
    fitEqBut.configure(text=fileFitEq.split("/")[-1])
    fileFitness[1] = fileFitEq


def getFit():
    """ This function gets the fitness file """
    fileFit = filedialog.askopenfilename()
    fitEqBut.configure(state="disabled")
    fitBut.configure(text=fileFit.split("/")[-1])
    fileFitness[0] = fileFit


def colClicked(event):
    """ This function is responsible of the display of colonization tab """
    colRate.configure(state='disabled')
    colDirect.configure(state='disabled')
    colnEdge.configure(state='disabled')
    colnClust.configure(state='disabled')
    colProbIntra.configure(state='disabled')
    colProbInter.configure(state='disabled')
    colPower.configure(state='disabled')
    colNetFileBut.configure(state='disabled')
    colTrans.configure(state='disabled')
    colFromOne.configure(state='disabled')

    if colNet.get().split(":")[0] > "0":
        colRate.configure(state='enabled')
        colNetFileBut.configure(state='enabled')
        colTrans.configure(state='enabled')
        colFromOne.configure(state='enabled')

        if colNet.get().split(":")[0] > "2":
            colDirect.configure(state='enabled')

            if colNet.get().split(":")[0] == "4":
                colnEdge.configure(state='enabled')

            elif colNet.get().split(":")[0] == "5":
                colnClust.configure(state='enabled')
                colProbIntra.configure(state='enabled')
                colProbInter.configure(state='enabled')
                colPower.configure(state='disabled')
                colnEdge.configure(state='disabled')

            if colNet.get().split(":")[0] == "6":
                colPower.configure(state='enabled')
                colnClust.configure(state='disabled')
                colProbIntra.configure(state='disabled')
                colProbInter.configure(state='disabled')


def migClicked(event):
    """ This function is responsible of the display of migration tab """

    migRate.configure(state='disabled')
    migDirect.configure(state='disabled')
    mignEdge.configure(state='disabled')
    mignClust.configure(state='disabled')
    migProbIntra.configure(state='disabled')
    migProbInter.configure(state='disabled')
    migPower.configure(state='disabled')
    migNetFileBut.configure(state='disabled')
    migTrans.configure(state='disabled')
    migFromOne.configure(state='disabled')
    migCarr.configure(state='disabled')
    migRepl.configure(state='disabled')

    if migNet.get().split(":")[0] > "0":
        migRate.configure(state='enabled')
        migNetFileBut.configure(state='enabled')
        migTrans.configure(state='enabled')
        migFromOne.configure(state='enabled')
        migCarr.configure(state='enabled')
        migRepl.configure(state='enabled')

        if migNet.get().split(":")[0] > "2":
            migDirect.configure(state='enabled')

            if migNet.get().split(":")[0] == "4":
                mignEdge.configure(state='enabled')

            elif migNet.get().split(":")[0] == "5":
                mignClust.configure(state='enabled')
                migProbIntra.configure(state='enabled')
                migProbInter.configure(state='enabled')
                migPower.configure(state='disabled')
                mignEdge.configure(state='disabled')

            if migNet.get().split(":")[0] == "6":
                migPower.configure(state='enabled')
                mignClust.configure(state='disabled')
                migProbIntra.configure(state='disabled')
                migProbInter.configure(state='disabled')

window = Tk()

window.title("Welcome to CMPconfig")
# window.geometry('350x500')

tabs = ttk.Notebook(window)
tabSimu = ttk.Frame(tabs)
tabMeta = ttk.Frame(tabs)
tabBreed = ttk.Frame(tabs)
tabExt = ttk.Frame(tabs)
tabColo = ttk.Frame(tabs)
tabMigr = ttk.Frame(tabs)
tabOut = ttk.Frame(tabs)
tabSubmit = ttk.Frame(tabs)
tabs.add(tabSimu, text="Simulation")
tabs.add(tabMeta, text="Meta-population")
tabs.add(tabBreed, text="Breeding")
tabs.add(tabExt, text="Extinction")
tabs.add(tabColo, text="Colonization")
tabs.add(tabMigr, text="Migration")
tabs.add(tabOut, text="Outputs")
tabs.add(tabSubmit, text="Submit")


# The simulation parameters to set


lblName = Label(tabSimu, text="Folder name")
lblName.grid(row=0, column=0, padx=5, pady=5)
name = Entry(tabSimu, width=10)
name.grid(row=0, column=1, padx=5, pady=5)

foldTime = IntVar()
foldTime.set(False)
foldTimeButton = Checkbutton(tabSimu, text="Include folder time", var=foldTime)
foldTimeButton.grid(row=1, column=0, padx=5, pady=5)

lblGen = Label(tabSimu, text="Number of generation")
lblGen.grid(row=2, column=0, padx=5, pady=5)
generation = Entry(tabSimu, width=10)
generation.grid(row=2, column=1, padx=5, pady=5)

lblRep = Label(tabSimu, text="Number of replicates")
lblRep.grid(row=3, column=0, padx=5, pady=5)
replicates = Entry(tabSimu, width=10)
replicates.grid(row=3, column=1, padx=5, pady=5)

lblSeed = Label(tabSimu, text="Seed of simulations")
lblSeed.grid(row=4, column=0, padx=5, pady=5)
seed = Entry(tabSimu, width=10)
seed.grid(row=4, column=1, padx=5, pady=5)


# The Meta-population parameters to set


lblnPop = Label(tabMeta, text="Population number")
lblnPop.grid(row=0, column=0, padx=5, pady=5)
nPop = Entry(tabMeta, width=10)
nPop.grid(row=0, column=1, padx=5, pady=5)

lblinitSize = Label(tabMeta, text="Initial size of pops")
lblinitSize.grid(row=1, column=0, padx=5, pady=5)
initSize = Entry(tabMeta, width=10)
initSize.grid(row=1, column=1, padx=5, pady=5)

lblnMark = Label(tabMeta, text="Number of markers")
lblnMark.grid(row=2, column=0, padx=5, pady=5)
nMark = Entry(tabMeta, width=10)
nMark.grid(row=2, column=1, padx=5, pady=5)

lblnAlle = Label(tabMeta, text="Number of alleles")
lblnAlle.grid(row=3, column=0, padx=5, pady=5)
nAlle = Entry(tabMeta, width=10)
nAlle.grid(row=3, column=1, padx=5, pady=5)

lblCarrCap = Label(tabMeta, text="Carrying capacity")
lblCarrCap.grid(row=4, column=0, padx=5, pady=5)
carrCap = Entry(tabMeta, width=10)
carrCap.insert(END, "1000")
carrCap.grid(row=4, column=1, padx=5, pady=5)

lblnAlleFreq = Label(tabMeta, text="Select file for\ninit_AlleleFrequency")
lblnAlleFreq.grid(row=5, column=0, padx=5, pady=5)
initAlleFreq = Button(tabMeta, text="Click here", command=getInitAlleFreq)
initAlleFreq.grid(row=5, column=1, padx=5, pady=5)

lblnAlleFreqEq = Label(tabMeta, text="Select file for\ninit_AlleleFrequency_equal")
lblnAlleFreqEq.grid(row=6, column=0, padx=5, pady=5)
initAlleFreqEq = Button(tabMeta, text="Click here", command=getInitAlleFreq)
initAlleFreqEq.grid(row=6, column=1, padx=5, pady=5)

lblnGenoFreq = Label(tabMeta, text="Select file for\ninit_GenotypeFrequency")
lblnGenoFreq.grid(row=7, column=0, padx=5, pady=5)
initGenoFreq = Button(tabMeta, text="Click here", command=getInitGenoFreq)
initGenoFreq.grid(row=7, column=1, padx=5, pady=5)

lblGenoFreqEq = Label(tabMeta, text="Select file for\ninit_GenotypeFrequency_equal")
lblGenoFreqEq.grid(row=8, column=0, padx=5, pady=5)
initGenoFreqEq = Button(tabMeta, text="Click here", command=getInitGenoFreq)
initGenoFreqEq.grid(row=8, column=1, padx=5, pady=5)

lblGenetMap = Label(tabMeta, text="Select file for\nGenetic Map")
lblGenetMap.grid(row=9, column=0, padx=5, pady=5)
genetMapBut = Button(tabMeta, text="Click here", command=getGenetMap)
genetMapBut.grid(row=9, column=1, padx=5, pady=5)


# The Breeding parameters to set


lblFec = Label(tabBreed, text="Fecundity")
lblFec.grid(row=0, column=0, padx=5, pady=5)
fecund = Entry(tabBreed, width=10)
fecund.grid(row=0, column=1, padx=5, pady=5)

lblSelf = Label(tabBreed, text="percent Self")
lblSelf.grid(row=1, column=0, padx=5, pady=5)
percSelf = Entry(tabBreed, width=10)
percSelf.grid(row=1, column=1, padx=5, pady=5)

lblMutRate = Label(tabBreed, text="Mutation rate")
lblMutRate.grid(row=2, column=0, padx=5, pady=5)
mutRate = Entry(tabBreed, width=10)
mutRate.grid(row=2, column=1, padx=5, pady=5)

lblOptim = Label(tabBreed, text="Optimum")
lblOptim.grid(row=3, column=0, padx=5, pady=5)
optim = Entry(tabBreed, width=10)
optim.grid(row=3, column=1, padx=5, pady=5)

lblFitEq = Label(tabBreed, text="Select file for\nfitness_equal")
lblFitEq.grid(row=4, column=0, padx=5, pady=5)
fitEqBut = Button(tabBreed, text="Click here", command=getFitEq)
fitEqBut.grid(row=4, column=1, padx=5, pady=5)

lblFit = Label(tabBreed, text="Select file for\nfitness")
lblFit.grid(row=5, column=0, padx=5, pady=5)
fitBut = Button(tabBreed, text="Click here", command=getFit)
fitBut.grid(row=5, column=1, padx=5, pady=5)


# The Extinction parameter

lblExt = Label(tabExt, text="Extinciton rate")
lblExt.grid(row=0, column=0, padx=5, pady=5)
extRate = Entry(tabExt, width=10)
extRate.grid(row=0, column=1, padx=5, pady=5)


# The colonization parameters

lblColNet = Label(tabMigr, text="Colonization network type")
lblColNet.grid(row=0, column=0, padx=5, pady=5)
colNet = Combobox(tabColo)
colNet['values'] = ("0:No colonization", "1:Stepping stone 1D", "2:Stepping stone 2D", "3:Island model", "4:Erdos-Renyi model", "5:Community model", "6:Barabasi model")
colNet.current(0)
colNet.grid(row=0, column=1, padx=5, pady=5)
colNet.bind("<<ComboboxSelected>>", colClicked)

lblColRate = Label(tabColo, text="Colonization rate")
lblColRate.grid(row=1, column=0, padx=5, pady=5)
colRate = Entry(tabColo, width=10, state='disabled')
colRate.grid(row=1, column=1, padx=5, pady=5)

lblColDirect = Label(tabColo, text="Directed network Colonization")
lblColDirect.grid(row=2, column=0, padx=5, pady=5)
colDirect = Combobox(tabColo, state='disabled')
colDirect['values'] = ("0:Not directed", "1:Directed")
colDirect.current(0)
colDirect.grid(row=2, column=1, padx=5, pady=5)

lblColnEdge = Label(tabColo, text="Number of edges")
lblColnEdge.grid(row=3, column=0, padx=5, pady=5)
colnEdge = Entry(tabColo, width=10, state='disabled')
colnEdge.grid(row=3, column=1, padx=5, pady=5)

lblColnClust = Label(tabColo, text="Number of cluster")
lblColnClust.grid(row=4, column=0, padx=5, pady=5)
colnClust = Entry(tabColo, width=10, state='disabled')
colnClust.grid(row=4, column=1, padx=5, pady=5)

lblColProbIntra = Label(tabColo, text="Probability of connection\nintra-cluster")
lblColProbIntra.grid(row=5, column=0, padx=5, pady=5)
colProbIntra = Entry(tabColo, width=10, state='disabled')
colProbIntra.grid(row=5, column=1, padx=5, pady=5)

lblColProbInter = Label(tabColo, text="Probability of connection\ninter-cluster")
lblColProbInter.grid(row=6, column=0, padx=5, pady=5)
colProbInter = Entry(tabColo, width=10, state='disabled')
colProbInter.grid(row=6, column=1, padx=5, pady=5)

lblColPower = Label(tabColo, text="Power of Barabasi")
lblColPower.grid(row=7, column=0, padx=5, pady=5)
colPower = Entry(tabColo, width=10, state='disabled')
colPower.grid(row=7, column=1, padx=5, pady=5)

lblColNetFile = Label(tabColo, text="Select file for colonization")
lblColNetFile.grid(row=8, column=0, padx=5, pady=5)
colNetFileBut = Button(tabColo, text="Select file", command=getColFile, state='disabled')
colNetFileBut.grid(row=8, column=1, padx=5, pady=5)

lblColTrans = Label(tabColo, text="Select colonization\ntransfer model")
lblColTrans.grid(row=9, column=0, padx=5, pady=5)
colTrans = Combobox(tabColo, state='disabled')
colTrans['values'] = ("excess", "friendly")
colTrans.current(0)
colTrans.grid(row=9, column=1, padx=5, pady=5)

lblColFromOne = Label(tabColo, text="Select col_from_one")
lblColFromOne.grid(row=10, column=0, padx=5, pady=5)
colFromOne = Combobox(tabColo, state='disabled')
colFromOne['values'] = ("0", "1")
colFromOne.current(0)
colFromOne.grid(row=10, column=1, padx=5, pady=5)


# The migration parameters

lblMigNet = Label(tabMigr, text="Migration network type")
lblMigNet.grid(row=0, column=0, padx=5, pady=5)
migNet = Combobox(tabMigr)
migNet['values'] = ("0:No migration", "1:Stepping stone 1D", "2:Stepping stone 2D", "3:Island model", "4:Erdos-Renyi model", "5:Community model", "6:Barabasi model")
migNet.current(0)
migNet.grid(row=0, column=1, padx=5, pady=5)
migNet.bind("<<ComboboxSelected>>", migClicked)

lblMigRate = Label(tabMigr, text="Migration rate")
lblMigRate.grid(row=1, column=0, padx=5, pady=5)
migRate = Entry(tabMigr, width=10, state='disabled')
migRate.grid(row=1, column=1, padx=5, pady=5)

lblMigDirect = Label(tabMigr, text="Directed network Migration")
lblMigDirect.grid(row=2, column=0, padx=5, pady=5)
migDirect = Combobox(tabMigr, state='disabled')
migDirect['values'] = ("0:Not directed", "1:Directed")
migDirect.current(0)
migDirect.grid(row=2, column=1, padx=5, pady=5)

lblMignEdge = Label(tabMigr, text="Number of edges")
lblMignEdge.grid(row=3, column=0, padx=5, pady=5)
mignEdge = Entry(tabMigr, width=10, state='disabled')
mignEdge.grid(row=3, column=1, padx=5, pady=5)

lblMignClust = Label(tabMigr, text="Number of cluster")
lblMignClust.grid(row=4, column=0, padx=5, pady=5)
mignClust = Entry(tabMigr, width=10, state='disabled')
mignClust.grid(row=4, column=1, padx=5, pady=5)

lblMigProbIntra = Label(tabMigr, text="Probability of connection\nintra-cluster")
lblMigProbIntra.grid(row=5, column=0, padx=5, pady=5)
migProbIntra = Entry(tabMigr, width=10, state='disabled')
migProbIntra.grid(row=5, column=1, padx=5, pady=5)

lblMigProbInter = Label(tabMigr, text="Probability of connection\ninter-cluster")
lblMigProbInter.grid(row=6, column=0, padx=5, pady=5)
migProbInter = Entry(tabMigr, width=10, state='disabled')
migProbInter.grid(row=6, column=1, padx=5, pady=5)

lblMigPower = Label(tabMigr, text="Power of Barabasi")
lblMigPower.grid(row=7, column=0, padx=5, pady=5)
migPower = Entry(tabMigr, width=10, state='disabled')
migPower.grid(row=7, column=1, padx=5, pady=5)

lblMigNetFile = Label(tabMigr, text="Select file for migration")
lblMigNetFile.grid(row=8, column=0, padx=5, pady=5)
migNetFileBut = Button(tabMigr, text="Select file", command=getMigFile, state='disabled')
migNetFileBut.grid(row=8, column=1, padx=5, pady=5)

lblMigTrans = Label(tabMigr, text="Select migration\ntransfer model")
lblMigTrans.grid(row=9, column=0, padx=5, pady=5)
migTrans = Combobox(tabMigr, state='disabled')
migTrans['values'] = ("excess", "friendly")
migTrans.current(0)
migTrans.grid(row=9, column=1, padx=5, pady=5)

lblMigFromOne = Label(tabMigr, text="Select migr_from_one")
lblMigFromOne.grid(row=10, column=0, padx=5, pady=5)
migFromOne = Combobox(tabMigr, state='disabled')
migFromOne['values'] = ("0", "1")
migFromOne.current(0)
migFromOne.grid(row=10, column=1, padx=5, pady=5)

lblMigCarr = Label(tabMigr, text="Select migr__carrying")
lblMigCarr.grid(row=11, column=0, padx=5, pady=5)
migCarr = Combobox(tabMigr, state='disabled')
migCarr['values'] = ("0:Be at carrying capacity", "1:Don't need to be at carr_cap")
migCarr.current(0)
migCarr.grid(row=11, column=1, padx=5, pady=5)

lblMigRepl = Label(tabMigr, text="Migration_replace")
lblMigRepl.grid(row=12, column=0, padx=5, pady=5)
migRepl = Entry(tabMigr, width=10, state='disabled')
migRepl.grid(row=12, column=1, padx=5, pady=5)


# The output parameters

lblstep = Label(tabOut, text="Steps to record")
lblstep.grid(row=0, column=0, padx=5, pady=5)
step = Entry(tabOut, width=10)
step.grid(row=0, column=1, padx=5, pady=5)

outGeno = IntVar()
outGeno.set(0)
outHaplo = IntVar()
outHaplo.set(0)
outSeedTrans = IntVar()
outSeedTrans.set(0)

outGenoBox = Checkbutton(tabOut, text="Genotype", var=outGeno)
outGenoBox.grid(row=1, column=0, padx=5, pady=5)
outHaploBox = Checkbutton(tabOut, text="Haplotype", var=outHaplo)
outHaploBox.grid(row=2, column=0, padx=5, pady=5)
outSeedTransBox = Checkbutton(tabOut, text="Seed transfer", var=outSeedTrans)
outSeedTransBox.grid(row=3, column=0, padx=5, pady=5)

lblSepRep = Label(tabOut, text="Select separate_replicate")
lblSepRep.grid(row=4, column=0, padx=5, pady=5)
sepRep = Combobox(tabOut)
sepRep['values'] = ("0:All in one file", "1:In separate files")
sepRep.current(0)
sepRep.grid(row=4, column=1, padx=5, pady=5)


# The submit tab

submit = Button(tabSubmit, text="Submit", command=createString)
submit.grid(row=0, column=0, padx=5, pady=5)

tabs.pack(expand=1, fill='both')

window.mainloop()
